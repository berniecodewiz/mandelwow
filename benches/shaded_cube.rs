#![feature(test)]
extern crate test;

use glium::uniform;
use mandelwow_lib::shaded_cube::*;
use std::rc::Rc;

#[bench]
fn bench_shaded_cube(b: &mut test::Bencher) {
    let event_loop = winit::event_loop::EventLoopBuilder::new()
        .build()
        .expect("event loop building");
    let (_window, display) = glium::backend::glutin::SimpleWindowBuilder::new()
        .build(&event_loop);

    let program = Rc::new(shaded_program(&display));
    let cube = ShadedCube::new(&display, program);
    let mut frame = display.draw();
    b.iter(|| {
        let model =     [[0.7, 0.5, -0.5, 0.0], [0.0, 0.7, 0.7, 0.0], [0.7, -0.5,  0.5,  0.0], [0., 0., -3.0, 1.0f32]];
        let perspview = [[0.5, 0.0,  0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [0.0,  0.0, -1.0, -1.0], [0., 0., -0.2, 0.0f32]];
        let uniforms = uniform! {
            model: model,
            perspview: perspview,
        };
        cube.draw(&mut frame, &uniforms);
    });
    frame.finish().unwrap();
}
